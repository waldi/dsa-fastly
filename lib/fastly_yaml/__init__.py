import fastly
import string
import os


def fastly_url_quote(name):
    return name.replace(" ", "%20").replace("/", "%2F")


# Director type is defined by an integer towards the API, which can cause confusion in our config
def to_director_type_integer(type_string):
    return fastly.FastlyDirectorType.__dict__[string.upper(type_string)]


class FastlyConfig(object):
    def __init__(self, client, dry_run=True, logger=None):
        self.client = client
        self.logger = logger
        self.dry_run = dry_run

    def _manage(self, descriptive_name, kwarg_names,
                fastly_side, yaml_side, create, update, delete):
        self.logger.info("Managing %s", descriptive_name)
        # Delete
        for fs_item in fastly_side.values():
            self.logger.debug("Considering deletion of %s %s", descriptive_name, fs_item)
            y_items = filter(lambda x: x['name'] == fs_item.name, yaml_side)
            if not y_items:
                if self.dry_run:
                    self.logger.info("Would delete, but skipping because dry_run %s %s", descriptive_name, fs_item.name)
                else:
                    self.logger.info("Deleting %s %s", descriptive_name, fs_item.name)
                    delete(self.service_id, self.service_version, fastly_url_quote(fs_item.name))
        # Create
        for y_item in yaml_side:
            if y_item['name'] in fastly_side:
                continue
            if self.dry_run:
                self.logger.info("Would create new %s %s", descriptive_name, y_item)
            else:
                kwargs = dict([(x, y_item[x],) for x in kwarg_names if y_item.get(x) is not None])
                self.logger.info("Creating new %s %s", descriptive_name, y_item)
                self.logger.debug(
                    "create({}, {}, {}, {})".format(self.service_id, self.service_version, y_item['name'], kwargs))
                fastly_side[y_item['name']] = create(self.service_id, self.service_version, y_item['name'], **kwargs)
        # Update
        for fs_item in fastly_side.values():
            self.logger.debug("Considering update of %s %s", descriptive_name, fs_item)
            y_items = filter(lambda x: x['name'] == fs_item.name, yaml_side)
            if not y_items:
                continue
            y_item = y_items[0]
            needs_update = False
            # Item exists, now to compare settings.  We know name matches
            for setting in kwarg_names:
                if setting not in y_item:
                    continue
                fs_setting_name = setting
                if setting.startswith("_"):
                    fs_setting_name = setting[1:]
                    self.logger.debug("getting %s from %s", fs_setting_name, fs_item)
                try:
                    fastly_setting = getattr(fs_item, fs_setting_name)
                except AttributeError:
                    self.logger.exception(
                        "Failed to get setting name %s from item %s. This might be because %s is an illegal name",
                        fs_setting_name, fs_item, fs_setting_name)
                    raise
                if not (isinstance(fastly_setting, str) or isinstance(fastly_setting, unicode) or
                        isinstance(fastly_setting, int) or fastly_setting is None):
                    fastly_setting = fastly_setting.name
                if y_item[setting] != fastly_setting:
                    self.logger.info("Setting %s is different: '%s' != '%s'", setting, y_item[setting],
                                     fastly_setting)
                    needs_update = True
            if needs_update:
                if self.dry_run:
                    self.logger.info("Not updating %s %s due to dry_run", descriptive_name, fs_item.name)
                else:
                    self.logger.info("Updating %s %s", descriptive_name, fs_item.name)
                    kwargs = dict([(x, y_item[x],) for x in kwarg_names if y_item.get(x)])
                    self.logger.debug("update({}, {}, {}, {})".format(self.service_id, self.service_version,
                                                                      fastly_url_quote(fs_item.name), kwargs))
                    update(self.service_id, self.service_version, fastly_url_quote(fs_item.name), **kwargs)

    def manage_domains(self, service_domains, yaml_domains):
        return self._manage("domains", ['comment'],
                            service_domains, yaml_domains,
                            self.client.create_domain,
                            self.client.update_domain,
                            self.client.delete_domain)

    def manage_healthchecks(self, service_healthchecks, yaml_healthchecks):
        kwarg_names = ["host", "method", "path", "http_version", "timeout", "check_interval",
                       "expected_response", "window", "threshold", "initial"]
        return self._manage("healthchecks",
                            kwarg_names, service_healthchecks,
                            yaml_healthchecks,
                            self.client.create_healthcheck,
                            self.client.update_healthcheck,
                            self.client.delete_healthcheck)

    def manage_backends(self, fs_side, yaml_side):
        kwarg_names = ["address",
                       "port",
                       "use_ssl",
                       "connect_timeout",
                       "first_byte_timeout",
                       "between_bytes_timeout",
                       "error_threshold",
                       "max_conn",
                       "weight",
                       "auto_loadbalance",
                       "shield",
                       "request_condition",
                       "healthcheck",
                       "comment",
                       "ssl_cert_hostname",
                       "ssl_sni_hostname",
                       ]
        return self._manage("backends", kwarg_names,
                            fs_side, yaml_side,
                            self.client.create_backend,
                            self.client.update_backend,
                            self.client.delete_backend)

    def manage_directors(self, fs_side, yaml_side):
        kwarg_names = ["_type",
                       "comment",
                       "quorum",
                       "shield",
                       "retries",
                       ]
        # fs_side = dict([ (s.name, s) for s in self.client.list_directors(self.service_id, self.service_version)])
        for y_item in yaml_side:
            if "type" in y_item:
                y_item['_type'] = to_director_type_integer(y_item['type'])
        self._manage("directors", kwarg_names,
                     fs_side, yaml_side,
                     self.client.create_director,
                     self.client.update_director,
                     self.client.delete_director)

        for director in yaml_side:
            name = director['name']
            self.logger.debug("fastly side: %s", fs_side[name].backends)
            self.logger.debug("yaml: %s", set(director['backends']))
            backends_to_remove = set(fs_side[name].backends) - set(director['backends'])
            backends_to_add = set(director['backends']) - set(fs_side[name].backends)
            for backend in backends_to_remove:
                self.client.delete_director_backend(self.service_id, self.service_version, name, backend)
            for backend in backends_to_add:
                self.client.create_director_backend(self.service_id, self.service_version, name, backend)

    def manage_request_settings(self, yaml_side):
        kwarg_names = ["action",
                       "bypass_busy_wait",
                       "default_host",
                       "force_miss",
                       "force_ssl",
                       "geo_headers",
                       "hash_keys",
                       "max_stale_age",
                       "request_condition",
                       "service_id",
                       "timer_support",
                       "version",
                       "xff"
                       ]
        fs_side = dict([(s.name, s) for s in self.client.list_request_settings(self.service_id, self.service_version)])
        return self._manage("request settings",
                            kwarg_names, fs_side, yaml_side,
                            self.client.create_request_setting,
                            self.client.update_request_setting,
                            self.client.delete_request_setting)

    def manage_cache_settings(self, yaml_side):
        kwarg_names = ["action",
                       "ttl",
                       "stale_ttl",
                       "cache_condition",
                       ]
        fs_side = dict([(s.name, s) for s in self.client.list_cache_settings(self.service_id, self.service_version)])
        return self._manage("cache settings",
                            kwarg_names, fs_side, yaml_side,
                            self.client.create_cache_settings,
                            self.client.update_cache_settings,
                            self.client.delete_cache_settings)

    def manage_conditions(self, yaml_side):
        kwarg_names = ["_type",
                       "statement",
                       "priority",
                       "comment"
                       ]
        fs_side = dict([(s.name, s) for s in self.client.list_conditions(self.service_id, self.service_version)])
        for y_item in yaml_side:
            if "type" in y_item:
                y_item['_type'] = y_item['type']
        return self._manage("conditions", kwarg_names,
                            fs_side, yaml_side,
                            self.client.create_condition,
                            self.client.update_condition,
                            self.client.delete_condition)

    def manage_headers(self, yaml_side):
        kwarg_names = ["dst",
                       "src",
                       "_type",
                       "action",
                       "regex",
                       "substitution",
                       "ignore_if_set",
                       "priority",
                       "response_condition",
                       "request_condition",
                       "cache_condition",
                       ]
        fs_side = dict([(s.name, s) for s in self.client.list_headers(self.service_id, self.service_version)])
        for y_item in yaml_side:
            if "type" in y_item:
                y_item['_type'] = y_item['type']
            if 'destination' in y_item:
                y_item['dst'] = y_item['destination']
            if 'source' in y_item:
                y_item['src'] = y_item['source']
        return self._manage("headers", kwarg_names,
                            fs_side, yaml_side,
                            self.client.create_header,
                            self.client.update_header,
                            self.client.delete_header)

    def manage_response_objects(self, yaml_side):
        kwarg_names = ["status",
                       "response",
                       "content",
                       "request_condition",
                       "cache_condition",
                       ]
        fs_side = dict([(s.name, s) for s in self.client.list_response_objects(self.service_id, self.service_version)])

        return self._manage("response_objects", kwarg_names,
                            fs_side, yaml_side,
                            self.client.create_response_object,
                            self.client.update_response_object,
                            self.client.delete_response_object)

    def manage_vcls(self, yaml_side, filename):
        kwarg_names = ["content",
                       "main",
                       "comment",
                       ]
        for y in yaml_side:
            vclname = os.path.join(os.path.dirname(filename), y['file'])
            y['content'] = open(vclname).read()
        fs_side = dict([ (v.name, v) for v in self.client.list_vcls(self.service_id, self.service_version)])
        return self._manage("vcls", kwarg_names, fs_side, yaml_side,
                            self.client.upload_vcl,
                            self.client.update_vcl,
                            self.client.delete_vcl)

    def manage_syslogs(self, yaml_side):
        kwarg_names = ["address",
                       "port",
                       "use_tls",
                       "tls_ca_cert",
                       "token",
                       "_format",
                       "format",
                       "response_condition",
                       ]
        for y_item in yaml_side:
            if "format" in y_item:
                y_item['_format'] = y_item['format']
        fs_side = dict([ (v.name, v) for v in self.client.list_syslogs(self.service_id, self.service_version)])
        return self._manage("syslog", kwarg_names, fs_side, yaml_side,
                            self.client.create_syslog,
                            self.client.update_syslog,
                            self.client.delete_syslog)

    def update(self, conf, filename):
        for svc in self.client.list_services():
            s = filter(lambda x: x['name'] == svc.name, conf['services'])
            if not s:
                continue
            s = s[0]

            try:
                active_version = filter(lambda x: x['active'], svc.versions)[0]
            except IndexError:
                self.logger.error("No active configuration found for service: %s, skipping", svc.name)
                continue

            highest_unlocked_version = active_version
            try:
                highest_unlocked_version = max(filter(lambda x: not x['locked'], svc.versions),
                                               key=lambda x: x['number'])
            except ValueError:
                pass
            v = None
            if active_version['number'] >= highest_unlocked_version['number']:
                # Need to create a new unlocked version so we have something to edit
                self.logger.info("Creating new version to have something to edit")
                v = self.client.create_version(svc.id)
            else:
                v = self.client.get_version(svc.id, highest_unlocked_version['number'])

            # Order is:
            # - delete no-longer used objects
            # - create new objects (healthchecks, backends, etc)
            # - update objects

            # Order to do objects in are:
            # - services - needs implementing
            # - domains
            # - headers
            # - healthcheck
            # - conditions (cache, request and response conditions)
            # - request settings
            # - cache settings
            # - response objects
            # - syslogs - needs implementing
            # - backends

            self.service_id = svc.id
            self.service_version = v.number
            self.manage_domains(v.domains, s['domains'])
            self.manage_healthchecks(v.healthchecks, s.get('healthchecks',[]))
            self.manage_conditions(s.get('conditions', []))
            self.manage_backends(v.backends, s['backends'])
            self.manage_directors(v.directors, s.get('directors', []))
            self.manage_request_settings(s.get('request_settings', []))
            self.manage_cache_settings(s.get('cache_settings', []))
            self.manage_response_objects(s.get('response_objects', []))
            self.manage_headers(s.get('headers', []))
            self.manage_vcls(s.get('vcls', []), filename)
            self.manage_syslogs(s.get('syslogs', []))
